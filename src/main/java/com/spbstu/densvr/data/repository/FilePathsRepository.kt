package com.spbstu.densvr.data.repository

import java.io.File

class FilePathsRepository : IFilePathsRepository {

    override fun getFilePaths(path: String, predicate: (String) -> Boolean): List<String> {
        val filePaths = getFilePathsRecursive(path, emptySet())
                .filter { predicate(it) }
        return filePaths
    }

    private fun getFilePathsRecursive(path: String, images: Set<String>): Set<String> {

        val file: File = File(path)
        if (file.isFile) {
            return images + file.absolutePath
        }
        val listOfFiles: List<File> = file.listFiles()?.toList() ?: emptyList()

        if (listOfFiles.isEmpty()) {
            return emptySet()
        }

        return listOfFiles.map { getFilePathsRecursive(it.absolutePath, images) }
                .reduce { allPaths, foundPaths -> allPaths + foundPaths }
    }



}