package com.spbstu.densvr.data.repository

interface IFilePathsRepository {

    fun getFilePaths(path: String, predicate: (String) -> Boolean = { true }): List<String>

}