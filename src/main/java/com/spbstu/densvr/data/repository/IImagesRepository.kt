package com.spbstu.densvr.data.repository

import com.spbstu.densvr.domain.model.Image

interface IImagesRepository {

    fun getImage(path: String): Image?

    fun isImagePath(path: String): Boolean

}