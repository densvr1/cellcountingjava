package com.spbstu.densvr.data.repository

import com.spbstu.densvr.domain.model.Image
import org.opencv.imgcodecs.Imgcodecs
import java.io.File


class ImagesRepository : IImagesRepository {

    override fun getImage(path: String): Image? =
            Image(Imgcodecs.imread(path), path).takeIf { it.mat.cols() > 0 && it.mat.rows() > 0 }

    override fun isImagePath(path: String): Boolean {
        try {
            val file = File(path)
            if (!file.isFile || !file.exists()) {
                return false
            }
            return IMAGE_EXTS.contains(file.extension.toLowerCase())
        } catch (e: Exception) {
            return false
        }
    }

    companion object {

        val IMAGE_EXTS = setOf("jpeg", "jpg", "png", "tiff", "bmp")

    }

}