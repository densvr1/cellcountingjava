package com.spbstu.densvr.domain.interactor

import com.spbstu.densvr.data.repository.IFilePathsRepository
import com.spbstu.densvr.data.repository.IImagesRepository
import com.spbstu.densvr.domain.model.Image

class LoadImagesInteractor(private val imagesRepository: IImagesRepository,
                           private val filePathsRepository: IFilePathsRepository) {

    fun getImagePaths(path: String): List<String> =
            filePathsRepository.getFilePaths(path, { imagesRepository.isImagePath(it) })

    fun getImage(path: String): Image? = imagesRepository.getImage(path)


}