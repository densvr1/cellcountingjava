package com.spbstu.densvr.domain.interactor

import org.opencv.core.*
import org.opencv.core.Core.countNonZero
import org.opencv.core.CvType.CV_8UC1
import org.opencv.imgproc.Imgproc
import org.opencv.imgproc.Moments

class ProcessImageInteractor {

    fun processImage(src: Mat, showImage: (Mat) -> Unit) {

        val binMat = binarizeInRange(src,
                0.0, 53.0,
                0.0, 23.0,
                0.0, 51.0)

        Imgproc.erode(binMat, binMat, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, Size(3.0, 3.0)))


        val contours: List<MatOfPoint> = mutableListOf()
        Imgproc.findContours(binMat, contours, Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE)



        val binInfectedMat = binarizeInRange(src,
                0.0, 255.0,
                0.0, 230.0,
                0.0, 255.0)

        var healthyCount = 0
        var infectedCount = 0

        val contourMat = Mat.zeros(src.size(), CV_8UC1)

        val drawMat = src.clone()

        for (contour in contours) {

            val contourArea = Imgproc.contourArea(contour)
            if (contourArea < 10 * 10) {
                continue
            }

            contourMat.setTo(Scalar(0.0))

            Imgproc.drawContours(contourMat, mutableListOf(contour), 0, Scalar(255.0), -1)

            var infectedRatio = 0.0

            val maskMat = contourMat.mul(binInfectedMat)
            infectedRatio = countNonZero(maskMat).toDouble()
            infectedRatio /= contourArea;
            println(infectedRatio)
            val isInfected: Boolean = infectedRatio > 0.9

            //draw results
            val contourPoints = contour.toArray()
            for (j in 0 until contourPoints.size) {
                if (!isInfected) {
                    Imgproc.line(drawMat, contourPoints[j], contourPoints[(j + 1) % contourPoints.size],
                            Scalar(0.0, 255.0, 0.0), 2)
                } else {
                    Imgproc.line(drawMat, contourPoints[j], contourPoints[(j + 1) % contourPoints.size],
                            Scalar(0.0, 0.0, 255.0), 2)
                }
            }

            if (isInfected) {
                infectedCount++
            } else {
                healthyCount++
            }

            val center = computeCentroid(contour);
            if (isInfected) {
                Imgproc.putText(drawMat, "$infectedCount", center, 1, 1.5, Scalar(255.0, 0.0, 255.0), 2)
            } else {
                Imgproc.putText(drawMat, "$healthyCount", center, 1, 1.5, Scalar(255.0, 255.0, 0.0), 2)
            }
        }

        Imgproc.putText(drawMat, "$healthyCount healthy", Point(5.0, drawMat.rows() - 5.0), 1, 3.0, Scalar(255.0, 255.0, 255.0), 2);
        Imgproc.putText(drawMat, "$infectedCount infected", Point(5.0, drawMat.rows() - 70.0), 1, 3.0, Scalar(255.0, 255.0, 255.0), 2);


        //Imgcodecs.imwrite("1" + "_processed.jpeg", drawMat);

        showImage(drawMat)

    }

}

fun binarizeInRange(src: Mat, r0: Double, r1: Double, g0: Double, g1: Double, b0: Double, b1: Double): Mat {
    val dst = Mat()
    Core.inRange(src, Scalar(r0, g0, b0), Scalar(r1, g1, b1), dst)
    Core.subtract(dst.clone().setTo(Scalar(255.0, 255.0, 255.0)), dst, dst)

    return dst
}

fun computeCentroid(contour: MatOfPoint): Point {
    val m: Moments = Imgproc.moments(contour, true)
    return Point(m.m10 / m.m00, m.m01 / m.m00);
}


fun resize(scrMat: Mat, newWidth: Double, withInterpolation: Boolean = true): Mat {
    val dstMat = Mat()
    val interpolation = if (withInterpolation) 1 else 0
    Imgproc.resize(scrMat, dstMat, Size(newWidth, scrMat.rows() * newWidth / scrMat.cols()), 0.0, 0.0, interpolation)
    return dstMat
}

/*
for(i in 0..dst.cols() - 1) {
    println("${dst[100, i][0]}")
}*/