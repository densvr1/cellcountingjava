package com.spbstu.densvr.domain.model

import org.opencv.core.Mat

class Image(val mat: Mat, val name: String)