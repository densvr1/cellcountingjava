package com.spbstu.densvr.ui.frame

import com.spbstu.densvr.data.repository.FilePathsRepository
import com.spbstu.densvr.data.repository.ImagesRepository
import com.spbstu.densvr.domain.interactor.LoadImagesInteractor
import com.spbstu.densvr.domain.interactor.ProcessImageInteractor
import com.spbstu.densvr.ui.presenter.ImagePresenter
import com.spbstu.densvr.ui.view.ImageFrameView
import com.spbstu.densvr.ui.viewstate.ImageFrameViewState
import com.spbstu.densvr.utils.addOnKeyPressedListener
import java.awt.FlowLayout
import java.awt.Image
import javax.swing.ImageIcon
import javax.swing.JFrame
import javax.swing.JLabel

class ImageFrame(private val imagesPath: String) : ImageFrameView {

    private val frame: JFrame = JFrame()
    private val label = JLabel()

    //TODO inject me
    private val presenter: ImagePresenter = ImagePresenter(
            LoadImagesInteractor(ImagesRepository(), FilePathsRepository()),
            ProcessImageInteractor(),
            imagesPath,
            ImageFrameViewState()
    )

    init {
        frame.layout = FlowLayout()
        frame.add(label)
        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE

        frame.addOnKeyPressedListener { presenter.onKeyPressed(it.keyCode) }

        presenter.bind(this)
    }

    override fun setDisplayedName(name: String) {
        frame.title = name
    }

    override fun setImage(image: Image) {
        val icon = ImageIcon(image)
        frame.setSize(image.getWidth(null) + 50, image.getHeight(null) + 50)
        label.icon = icon

    }

    override fun show() {
        frame.isVisible = true
    }

}