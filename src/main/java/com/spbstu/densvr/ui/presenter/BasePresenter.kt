package com.spbstu.densvr.ui.presenter

import com.spbstu.densvr.ui.viewstate.BaseViewState
import com.spbstu.densvr.ui.view.BaseView


abstract class BasePresenter<View : BaseView, out ViewState : BaseViewState<View>>(
        protected val viewState: ViewState) {

    open fun bind(view: View) {
        viewState.bindView(view)
        viewState.restore()
    }

    open fun unbind() {
        viewState.unbindView()
    }

}
