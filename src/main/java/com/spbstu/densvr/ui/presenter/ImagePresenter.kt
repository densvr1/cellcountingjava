package com.spbstu.densvr.ui.presenter

import com.spbstu.densvr.domain.interactor.LoadImagesInteractor
import com.spbstu.densvr.domain.interactor.ProcessImageInteractor
import com.spbstu.densvr.domain.interactor.resize
import com.spbstu.densvr.domain.model.Image
import com.spbstu.densvr.ui.view.ImageFrameView
import com.spbstu.densvr.ui.viewstate.ImageFrameViewState
import com.spbstu.densvr.utils.mat2BufferedImage
import org.opencv.core.Mat

class ImagePresenter(private val loadImagesInteractor: LoadImagesInteractor,
                     private val processImageInteractor: ProcessImageInteractor,
                     private val imagesPath: String,
                     viewState: ImageFrameViewState)

    : BasePresenter<ImageFrameView, ImageFrameViewState>(viewState) {

    private val imagePaths: List<String> = loadImagesInteractor.getImagePaths(imagesPath)

    private var position: Int = 0

    init {
        showPage()
    }

    fun onKeyPressed(key: Int) {
        when (key) {
            LEFT_ARROW_KEY -> prevPage()
            RIGHT_ARROW_KEY -> nextPage()
            KEY_P -> processImage()
        }
    }

    private fun showPage(isBackwards: Boolean = false) {
        image?.let {
            viewState.setDisplayedName(displayedName)
            showImage(it.mat)
        } ?: if (!isBackwards) nextPage() else prevPage()

    }

    private fun prevPage() {
        position -= 1
        if (position < 0) {
            position = imagePaths.size - 1
        }
        showPage(true)
    }

    private fun nextPage() {
        position += 1
        if (position >= imagePaths.size) {
            position = 0
        }
        showPage()
    }

    private fun processImage() {
        image?.let {
            processImageInteractor.processImage(it.mat, this::showImage)
        }
    }

    private fun showImage(image: Mat) {
        viewState.setImage(mat2BufferedImage(resize(image, SHOW_IMAGE_WIDTH)))
    }

    private val displayedName: String
            get() = "(${position + 1}:${imagePaths.size}) - ${imagePaths[position]}"

    private val image: Image?
            get() = loadImagesInteractor.getImage(imagePaths[position])

    companion object {

        private val LEFT_ARROW_KEY = 37
        private val RIGHT_ARROW_KEY = 39
        private val KEY_P = 80

        val SHOW_IMAGE_WIDTH = 600.0

    }

}