package com.spbstu.densvr.ui.view

import java.awt.Image

interface ImageFrameView: BaseView {

    fun setDisplayedName(name: String)

    fun setImage(image: Image)

    fun show()

}