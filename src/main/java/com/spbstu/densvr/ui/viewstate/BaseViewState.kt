package com.spbstu.densvr.ui.viewstate

import com.spbstu.densvr.ui.view.BaseView

abstract class BaseViewState<View: BaseView>: BaseView {

    protected var view: View? = null

    fun bindView(view: View) {
        this.view = view;
    }

    fun unbindView() {
        view = null
    }

    abstract fun restore();

}