package com.spbstu.densvr.ui.viewstate

import com.spbstu.densvr.ui.view.ImageFrameView
import java.awt.Image

class ImageFrameViewState: BaseViewState<ImageFrameView>(), ImageFrameView {

    private var name: String? = null

    private var image: Image? = null

    private var isShown: Boolean = false


    override fun setDisplayedName(name: String) {
        this.name = name
        view?.setDisplayedName(name)
    }

    override fun setImage(image: Image) {
        this.image = image
        view?.setImage(image)
    }

    override fun show() {
        isShown = true
        view?.show()
    }

    override fun restore() {
        name?.let { setDisplayedName(it) }
        image?.let { setImage(it) }
        if (isShown) show()
    }
}