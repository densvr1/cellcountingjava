package com.spbstu.densvr.utils

import org.opencv.core.Mat
import java.awt.image.BufferedImage
import java.awt.image.DataBufferByte


fun mat2BufferedImage(m: Mat): BufferedImage {
    var type = BufferedImage.TYPE_BYTE_GRAY
    if (m.channels() > 1) {
        type = BufferedImage.TYPE_3BYTE_BGR

    }
    val bufferSize = m.channels() * m.cols() * m.rows()
    val b = ByteArray(bufferSize)
    m.get(0, 0, b) // get all the pixels
    val image = BufferedImage(m.cols(), m.rows(), type)
    val targetPixels = (image.raster.dataBuffer as DataBufferByte).data
    System.arraycopy(b, 0, targetPixels, 0, b.size)
    return image
}