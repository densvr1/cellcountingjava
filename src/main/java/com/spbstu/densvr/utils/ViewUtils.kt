package com.spbstu.densvr.utils

import java.awt.Frame
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent

fun Frame.addOnKeyPressedListener(onKeyPressed: (KeyEvent) -> Unit) {
    this.addKeyListener(object: KeyAdapter() {

        override fun keyPressed(e: KeyEvent?) {
            e?.let { onKeyPressed.invoke(e) }
        }
    })

}